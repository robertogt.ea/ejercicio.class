import javax.swing.JOptionPane;

public class MenuPrincipal
{
  Computadora computadora;
  public MenuPrincipal()
  {
    menu();
  }  
  public void menu()
  {
    char opcion;
    do{
      opcion=(JOptionPane.showInputDialog("*********************** Men� principal *******************************\n"+
      "a. Crear instancia de computadora sin valores.\n"+
      "b. Crear instancia de computadora con los valores necesarios para cada uno de sus atributos.\n"+
      "c. Modificar la placa de la computadora.\n"+
      "d. Modificar el modelo de la computadora.\n"+
      "e. Modificar el tama�o de la pantalla de la computadora.\n"+
      "f. Modificar el estado de la computadora.\n"+
      "g. Ver los datos de la computadora.\n"+
      "h. Salir.")).charAt(0);
    
    }while(opcion!='h');
  }
  public static void main(String args[])
  {
    MenuPrincipal menuPrincipal = new MenuPrincipal();
  }
}